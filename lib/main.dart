import 'dart:io';

import 'package:flutter/material.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);



  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String imagepath;

  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Column(
           children: [

             Expanded(
                 child: Container(
                   child: Padding(
                     padding: EdgeInsets.all(1.0),
                     child: Center(
                       child: _imageList(),
                     ),
                   ),
                 )
             )
           ],

        ),

    );
  }

  Widget _imageList()
  {
    return GestureDetector(
      child: Center(
        child: Image.asset("assets/watch.jpg"),

      ),
      onTap: ()
       async{
        print("Trying to detect image file");

        String file = await getImageFileFromAssets("watch.jpg");

        print(file);
        setState(() {
          imagepath = file;
        });

        detectLabels();
      },
    );

  }
  void showInSnackBar(String message)
  {
    _scaffoldkey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  Future<String> getImageFileFromAssets(String path) async{
    final byteData = await rootBundle.load('assets/$path');
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_vision';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${DateTime.now().microsecondsSinceEpoch.toString()}.jpg';
    final file = File(filePath);
    await file.writeAsBytes(byteData.buffer.asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

    return filePath;

  }

  Future<void> detectLabels() async{
    final FirebaseVisionImage visionImage = FirebaseVisionImage.fromFilePath(imagepath);
    final ImageLabeler labelDetector = FirebaseVision.instance.imageLabeler(
      ImageLabelerOptions(confidenceThreshold: 0.50)
    );
    final List<ImageLabel> labels = await labelDetector.processImage(visionImage);

    for(ImageLabel label in labels)
      {
        final String text = label.text;
        _scaffoldkey.currentState.showSnackBar(SnackBar(content: Text(text)));
      }
  }



}
